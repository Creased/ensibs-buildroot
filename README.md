# Composition d'un firmware personnalisé pour Raspberry Pi avec Buildroot

## Introduction

Buildroot est une suite de scripts (généralement des `Makefile`) permettant la
composition de systèmes GNU/Linux embarqués de manière automatisée.

Cette suite de composition permet d'aborder de façon très simple le fonctionnement
d'un système d'exploitation tout en constatant les différentes contraintes issues du
développement sur système embarqué.

Le présent document a pour objectif de présenter le fonctionnement de Buildroot
en composant un firmware personnalisé pour Raspberry Pi.

## Résumé des configurations

Les images ci-jointes (`sdcard.img` et `zImage`) correspondent au système d'exploitation
ainsi qu'à l'image disque pouvant être copiée bit à bit sur une carte SD afin de charger
le système d'exploitation issu de cette série de configurations.

Le script ci-joint (`upgrade`) est un script permettant de reflasher à la volée la
carte SD, à noter que tout le système de fichier, y compris de le *bootloader* sont
ici reflashés afin de garantir une mise à jour complète du système distant. Pour des
raisons d'optimisation, il est possible de mettre à jour uniquement le système en utilisant
un second script (`upgrade_zimage`) qui sera quant à lui capable d'offrir un accès beaucoup
plus rapide au nouveau système.

**<u>Par exemple&nbsp;:</u>**

```
$ ./upgrade /opt/buildroot/output/images/sdcard.img buildroot
# ou
$ ./upgrade_zimage /opt/buildroot/output/images/zImage buildroot
```

**<u>Remarque&nbsp;:</u>** l'utilisation de `buildroot` comme nom d'hôte est ici faite
en utilisant un alias via la configuration du client SSH (`~/.ssh/config`)&nbsp;:

```
Host buildroot
  Hostname 192.168.0.1
  Port 22
  User root
  IdentityFile ~/.ssh/root_rsa

```

L'authentification (à distance ou non) est possible en utilisant au choix le compte d'utilisateur
`root` ou `user`. Le choix a été fait ici d'autoriser l'authentification sur le compte `root`
pour des raisons de maintenance, il ne sera cependant pas conseillé de s'authentifier directement
avec cet utilisateur afin de réaliser des opérations de tests "standards" (p.ex., test de connexion)&nbsp;:

| Utilisateur | Mot de passe        | Clé privée   |
|-------------|---------------------|--------------|
| `user`      | `3ArN2TYh`          | Non définie  |
| `root`      | `J6fD0HBWmog99PYSq` | `root_rsa`   |

Le Raspberry Pi dispose d'une configuration réseau minimaliste ne lui permettant pas d'accéder
à Internet&nbsp;:

| IP            | Masque de sous-réseau |
|---------------|-----------------------|
| `192.168.0.1` | `255.255.255.0`       |

## Préparation du système

### Téléchargement de Buildroot

Buildroot n'étant rien de plus qu'une suite de scripts au format `Makefile`, aucune
installation n'est nécessaire. Ainsi, il suffit de télécharger les sources de Buildroot
depuis Github pour commencer à travailler&nbsp;:

```bash
$ git clone https://github.com/buildroot/buildroot
$ pushd buildroot/
```

### Configuration de Buildroot

Préparation du fichier de recette initiale pour correspondre
aux configurations spécifiques à une Raspberry Pi 3&nbsp;:

```bash
$ make raspberrypi3_defconfig
```

Configuration de Buildroot (modification du template)&nbsp;:

```bash
$ make menuconfig
```

Depuis l'interface de configuration ncurses, nous allons dans un premier temps
procéder à la génération du `initrd`&nbsp;:

```
Filesystem images ---> initial RAM filesystem linked into linux kernel
```

**<u>Note&nbsp;:</u>** le `initrd` ou *initial RAM filesystem* est une partition minimaliste
permettant l'amorçage complet du système d'exploitation en RAM. En utilisant ce type de
partition, il sera possible de mettre à jour le système d'exploitation à chaud puisqu'aucun accès
à la mémoire flash ne sera requis après démarrage complet du système.

**<u>Remarque&nbsp;:</u>** le système étant chargé en RAM, toutes modifications apportées
au système pendant son exécution ne seront pas enregistrées et seront uniquement accessible
en RAM (mémoire volatile). Il n'est donc pas nécessaire de conserver de partition racine
puisque celle-ci ne sera jamais exploitée et pourra donc être supprimée depuis le fichier de
configuration `board/raspberrypi3/genimage-raspberrypi3.cfg` (partition `rootfs`).
Cette commande permet de retirer automatiquement cette partition&nbsp;:

```bash
$ perl -p -i -e 'BEGIN{undef $/;} s/[\n\t\s]+partition rootfs \{[^\}]+\}//' board/raspberrypi3/genimage-raspberrypi3.cfg
```

Définition du mot de passe root&nbsp;:

```
System configuration ---> Root password
```

Définition de Linaro ARM comme toolchain&nbsp;:

```
Toolchain ---> Toolchain type ---> External toolchain
Toolchain ---> Toolchain (Linaro ARM 2018.05)
```

## Composition d'un système de base

### Compilation du root filesystem (rootfs)

Cette étape nécessite un accès réseau afin de télécharger les
différents paquets et sources à compiler pour composer notre
système&nbsp;:

```bash
$ make
```

**<u>Note&nbsp;:</u>** l'utilisation du compte root peut être bloquante à
certaines étapes, de plus, il s'agit d'une mauvaise pratique. Afin d'éviter
les erreurs, il suffit de définir la variable d'environnement `FORCE_UNSAFE_CONFIGURE`
ou d'opérer avec un utilisateur à faibles privilèges avant de tester la compilation&nbsp;:

```bash
$ export FORCE_UNSAFE_CONFIGURE=1
```

### Préparation de la carte SD

Une fois l'image compilée, on obtient une liste de fichiers
semblable à celle-ci&nbsp;:

```
    output/images
    +-- bcm2710-rpi-3-b.dtb
    +-- bcm2710-rpi-3-b-plus.dtb
    +-- bcm2710-rpi-cm3.dtb
    +-- boot.vfat
    +-- rootfs.cpio
    +-- rootfs.ext2
    +-- rootfs.ext4 -> rootfs.ext2
    +-- rpi-firmware
    │   +-- bootcode.bin
    │   +-- cmdline.txt
    │   +-- config.txt
    │   +-- fixup.dat
    │   +-- overlays
    │   `-- start.elf
    +-- sdcard.img
    `-- zImage
```

Le fichier qui nous intéresse ici est le fichier `sdcard.img` que nous allons
pouvoir directement copier bit à bit sur une carte SD&nbsp;:

```bash
$ export DEVICE=$(lsblk -al --output PKNAME | tail -n+2 | sort | perl -p -e 's/^\n$//' | uniq | grep -Eo "mmcblk[0-9]+")
$ dd bs=4M if=output/images/sdcard.img of=/dev/${DEVICE} conv=fsync
```

Une fois préparée, il suffit d'insérer la carte SD dans la Raspberry Pi
et l'alimenter pour tester le système composé.


### Séquence de boot du Raspberry Pi

Lorsque le Raspberry Pi va démarrer cela va se passer en 6
étapes :

#### Mise sous tension

Lors de sa mise sous tension, le Raspberry va démarrer seulement son GPU qui va s'occuper de charger la partition FAT de la carte SD afin d'accéder au fichier `bootcode.bin`

#### Exécution de Bootcode.bin

Le GPU exécute le fichier `bootcode.bin` qui va récupérer le fichier `start.elf`.

Le GPU va ensuite activer la SDRAM et transférer une copie de `start.elf` pour l'exécuter.

#### Exécution de Start.ELF

Le GPU exécute maintenant `start.elf`, celui-ci va répartir la mémoire entre le GPU et le CPU ARM selon la configuration présente dans `config.txt`.

Une fois ces étapes effectuées, `start.elf` va transférer `kernel.img` dans la partie basse de la mémoire réservée au CPU et va lire les arguments à passer au noyau dans le fichier `cmdline.txt`.

#### Exécution de Kernel.img

Cette fois c'est le CPU qui va exécuter `kernel.img` aprés que `start.elf` a activé le fonctionnement du CPU, le GPU assure dorénavant seulement le rôle de processeur graphique.

Le noyau se lance et Linux démarre.

#### Démarrage de Linux

Linux démarré, le noyau recherche le Root File System.

### Validation de fonctionnement du noyau

Pour valider le fonctionnement de notre système, nous devons au préalable
connecter un contrôleur UART série en respectant le schéma de câblage
suivant&nbsp;:

```
    +---+---+
    |   |   |  5V
    +---+---+                     +---+
    |   |   |  5V                 |   |
    +---+---+                     +---+
    |   |   | <---- Ground -----> |   |
    +---+---+                     +---+
    |   |   | <-- Data + (TX) --> |   | <-- Data – (RX)
    +---+---+                     +---+
    |   |   | <-- Data – (RX) --> |   | <-- Data + (TX)
    +---+---+                     +---+

      GPIO                         TTY
```

Pour prendre en main le système et valider son fonctionnement, nous allons
utiliser un client de communication série&nbsp;:

```bash
$ picocom -b 115200 -r -l /dev/ttyUSB0
```

La sortie standard devrait ressembler à ceci&nbsp;:

```
picocom v1.7

port is        : /dev/ttyUSB0
flowcontrol    : none
baudrate is    : 115200
parity is      : none
databits are   : 8
escape is      : C-a
local echo is  : no
noinit is      : no
noreset is     : yes
nolock is      : yes
send_cmd is    : sz -vv
receive_cmd is : rz -vv
imap is        : 
omap is        : 
emap is        : crcrlf,delbs,

Terminal ready

buildroot login: root
Password:
# id
uid=0(root) gid=0(root) groups=0(root),10(wheel)
```

Si tel n'est pas le cas, il faudra vérifier le raccordement des pins,
notamment les pins `RX` et `TX` qui peuvent avoir été inversés
par le constructeur de l'adapteur (le port `TX` d'un équipement est
raccordé au port `RX` de l'autre pour éviter les collisions).

## Composition d'un système avancé

### Ajout du service SSH

L'ajout d'un service SSH est possible en utilisant le paquet
[`dropbear`](https://matt.ucc.asn.au/dropbear/dropbear.html)&nbsp;:

```
Target packages ---> Networking applications ---> dropbear
```

### Création d'un overlayFS

Afin d'apporter des modifications à notre système, il est nécessaire
de recomposer l'image. En effet, la `rootfs` ayant été supprimée, les
modifications ne sont pas enregistrées sur une mémoire de masse (ou flash dans notre cas)
et sont stockées en mémoire vive.

#### Ajout d'une configuration réseau

La configuration réseau sur Linaro se fait dans le fichier `/etc/network/interfaces`,
cependant ce fichier n'est pas modifiable sur la carte SD (uniquement en RAM)
puisque nous ne disposons pas de la partition `rootfs`.

Afin d'ajouter ce fichier sur le système de destination, il est possible
d'utiliser un `OverlayFS`. Pour faire simple, le rôle de l'`OverlayFS` est de
fournir un ensemble de fichiers à monter (`binding`) sur le système distant lors du
démarrage de notre image système.

Préparation de l'`OverflayFS` et de la configuration réseau&nbsp;:

```bash
$ mkdir -p /opt/buildroot/overlayfs/etc/network/
$ cat <<-'EOF' >/opt/buildroot/overlayfs/etc/network/interfaces
# OverlayFS network configuration

auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
  pre-up /etc/network/nfs_check
  wait-delay 15
  address 192.168.0.1
  netmask 255.255.255.0

EOF
```

Création d'un `OverlayFS`&nbsp;:

```
System configuration ---> Root filesystem overlay directories ---> $(TOPDIR)/overlayfs
```

Il ne reste plus qu'à recomposer notre image système et la copier sur notre carte SD
pour pouvoir prendre le contrôle de notre Raspberry Pi à distance en utilisant le
protocole SSH&nbsp;:

```bash
$ ssh root@192.168.0.1
```

Vérifions le fichier de configuration réseau afin de s'assurer que le système a bien pris
en compte notre configuration et a chargé l'`OverlayFS`&nbsp;:

```bash
$ cat /etc/network/interfaces
```

On retrouve alors notre fichier préparé avant la composition de
notre système de fichier&nbsp;:

```
# OverlayFS network configuration

auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
  pre-up /etc/network/nfs_check
  wait-delay 15
  address 192.168.0.1
  netmask 255.255.255.0

```

### Mise à jour du Raspberry Pi à distance

Tel qu'évoqué précédemment, l'utilisation d'un `OverlayFS` permet de charger le système en RAM
et libère donc la mémoire flash de toute écriture ou lecture ultérieure au chargement complet
du système d'exploitation. Ainsi, afin de mettre à jour le firmware à distance, nous pouvons
simplement procéder à un `flashing` de la carte SD à chaud depuis le Raspberry Pi&nbsp;:

```bash
$ scp output/images/sdcard.img root@192.168.0.1:/tmp/
$ ssh root@192.168.0.1
```

Il ne reste plus qu'à flasher la carte SD "à chaud" depuis la session SSH en utilisant
la même procédure qu'utilisée précédemment&nbsp;:

```bash
$ export DEVICE=$(lsblk -al --output PKNAME | tail -n+2 | sort | perl -p -e 's/^\n$//' | uniq | grep -Eo "mmcblk[0-9]+")
$ dd bs=4M if=/tmp/sdcard.img of=/dev/${DEVICE} conv=fsync
```

Pour des raisons de simplicité, il est possible d'automatiser la procéder en utilisant un
script shell (voir fichier de script ci-joint)&nbsp;:

```bash
$ ./upgrade /opt/buildroot/output/images/sdcard.img 192.168.0.1
```

### Ajout d'un script de post-build

Afin d'identifier la version de notre système actuellement installé
sur notre carte SD, nous pouvons créer un fichier de post-build afin
d'ajouter un fichier texte contenant la version de notre système.

Définition du chemin d'accès au fichier de post-build&nbsp;:

```
System configuration  ---> Custom scripts to run before creating filesystem images ---> board/raspberrypi3/post-build.sh
```

Mise à jour du fichier de post-build afin de créer un fichier contenant
les informations d'identification de notre compilation&nbsp;:

```bash
$ cat <<-'_EOF_' >>board/raspberrypi3/post-build.sh

cat <<-EOF >${TARGET_DIR}/etc/build-id
Commit: $(git rev-parse HEAD)
Build time: $(date)

EOF

_EOF_
```

Une fois reflashée, l'image devrait alors contenir le fichier `/etc/build-id`, vérifions&nbsp;:

```bash
$ ssh root@192.168.0.1 cat /etc/build-id
```

Le résultat de cette dernière commande devrait être sensiblement identique à celui-ci&nbsp;:

```
Commit: 2989e3c87a1123350728904fd789ad513e76b073
Build time: lundi 24 septembre 2018, 17:56:57 (UTC+0200)

```

### Ajout d'un package dans Buildroot

Afin d'ajouter un package dans Buildroot, il faut tout d'abord ajouter une entrée dans
les fichiers de configuration de `ncurses` afin de proposer l'installation du package
depuis `make menuconfig`&nbsp;:

```bash
$ perl -p -i -e 's@(menu "Games"\n)@\1\tsource "package/ninvaders/Config.in"\n@' package/Config.in
```

**<u>Note&nbsp;:</u>** pour des raisons de cohérence, la nouvelle entrée correspondant à un jeu
sera ajoutée dans le menu `Games`.

Création du `Makefile` et du helper pour l'installation de `ninvaders` depuis `ncurses`&nbsp;:

```bash
$ mkdir package/ninvaders/
$ cat <<-'EOF' >package/ninvaders/Config.in
config BR2_PACKAGE_NINVADERS
	bool "ninvaders"
  select BR2_PACKAGE_NCURSES
	help
	  nInvaders is a Space Invaders clone based on ncurses for ASCII output.

	  https://sourceforge.net/projects/ninvaders/

EOF
$ cat <<-'EOF' >package/ninvaders/ninvaders.mk
################################################################################
#
# ninvaders
#
################################################################################

NINVADERS_VERSION = 0.1.1
NINVADERS_SITE = https://downloads.sourceforge.net/project/ninvaders/ninvaders/$(NINVADERS_VERSION)

NINVADERS_DEPENDENCIES = ncurses

define NINVADERS_BUILD_CMDS
    $(TARGET_MAKE_ENV) $(MAKE) $(TARGET_CONFIGURE_OPTS) -C $(@D) all
endef

define NINVADERS_INSTALL_TARGET_CMDS
    $(INSTALL) -m 0755 -D $(@D)/nInvaders $(TARGET_DIR)/usr/bin/ninvaders
endef

$(eval $(generic-package))

EOF
```

Une fois préparés, nous pouvons à présent ajouter le package `ninvaders` à notre
image depuis `make menuconfig`&nbsp;:

```
Target packages ---> Games ---> ninvaders (NEW)
```

Une fois reflashée, l'image devrait alors contenir notre package `ninvaders`, vérifions&nbsp;:

```bash
$ ssh root@192.168.0.1 TERM=xterm ninvaders
```

La commande précédente devrait alors démarrer le jeu `ninvaders`.

Afin de permettre le démarrage automatique du jeu au démarrage du système, il est
nécessaire de disposer d'un environnement permettant le démarrage d'un programme
exploitant un shell interactif. Cependant, un shell interactif n'est disponible
qu'après authentification et ouverture d'un VTY par un utilisateur. Nous allons
donc ajouter le programme dans le script d'ouverture de session utilisateur en
utilisant notre `OverlayFS`&nbsp;:

```bash
$ cp {output/target,overlayfs}/etc/profile
$ cat <<-'EOF' >>overlayfs/etc/profile
export TERM=xterm
ninvaders

EOF
```

Dorénavant, l'ouverture d'un shell interactif démarrera de façon systématique
le jeu `ninvaders`.

## Hardening

### Activation du RELRO

Un ELF (*Executable and Linkable Format*) lié dynamiquement exploite une table
de résolution appelée GOT (*Global Offset Table*) afin de résoudre de façon
dynamique les fonctions fournies par les bibliothèques partagées
(*shared libraries*).

En exploitant différentes vulnérabilités permettant l'écriture arbitraire de données
en mémoire, il est possible de réécrire le contenu de cette table afin de procéder
à une exécution de code arbitraire.

Afin d'empêcher cette réécriture, le mécanisme RELRO permet de rendre cette section
mémoire (`.got.plt`) accessible en lecture-seule lors de l'exécution du programme.

Pour activer le RELRO, il suffit d'activer l'option depuis le `make menuconfig`&nbsp;:

```
Build options ---> RELRO Protection ---> Full
```

### PIE

Le PIE (*Position-Independent Executable*) permet de rendre aléatoire l'adressage de 
la section `.text` d'un binaire afin de complexifier l'exploitation de vulnérabilités
pour l'écriture et l'exécution de code arbitraire.

Actuellement, il ne semble pas possible d'ajouter de façon systématique la PIE sur
l'ensemble des packages à compiler...

### Stack Smashing Protection

Le mécanisme de SSP (*Stack Smashing Protection*) permet d'empêcher l'écrasement de données
sur la stack en ajoutant un `canary` (valeur aléatoire, écrite sur la stack et vérifiée avant
chaque instructions `ret` via la méthode `__stack_chk_fail`).

Pour activer la SSP, il suffit d'activer l'option depuis le `make menuconfig`&nbsp;:

```
Build options ---> Stack Smashing Protection ---> -fstack-protector-all
```

### Buffer-overflow Detection

La détection de *buffer-overflow* peut se faire au moyen de l'activation de l'option `FORTIFY
SOURCES`. Le concept derrière cette protection est l'ajout d'un *wrapper* afin de vérifier les
données fournies aux fonctions permettant généralement de réaliser un dépassement de tampon.

Pour activer la *Buffer-overflow Detection*, il suffit d'activer l'option depuis le `make menuconfig`&nbsp;:

```
Build options ---> Buffer-overflow Detection (FORTIFY_SOURCE) ---> Aggressive
```

### Mise à jour du password encoding algorithm

Par défaut, Buildroot va exploiter l'algorithme `md5` pour le stockage
des mots de passe utilisateur, cependant, cet algorithme est vivement déconseillé
lorsque l'on souhaite assurer la sécurité de nos identifiants.

Pour activer l'algorithme `SHA-512`, il suffit d'activer l'option depuis le `make menuconfig`&nbsp;:

```
System configuration ---> Passwords encoding ---> sha-512
```

**<u>Note&nbsp;:</u>** l'utilisation d'un mot de passe fort sera tout de même nécessaire
afin de garantir la sécurité de l'accès à notre système.

### Création d'un utilisateur simple

L'utilisation du compte `root` étant déconseillée, il est possible d'ajouter un utilisateur
au système en spécifiant une liste d'utilisateurs à créer à Buildroot&nbsp;:

```
System configuration ---> Path to the users tables ---> $(TOPDIR)/usertable.txt
```

Création de la liste d'utilisateurs&nbsp;:

```bash
$ cat <<-'EOF' >usertable.txt
# username uid group gid password home shell groups comment

user -1 user -1 =3ArN2TYh /home/user /bin/sh - simple user

EOF
```

### Désactivation de l'authentification pour root

Par défaut, il est possible de s'authentifier en utilisant l'utilisateur `root`, ceci
est vivement déconseillé.

Pour désactiver l'authentification avec le compte d'utilisateur `root`, il suffit de désactiver l'option depuis le `make menuconfig`&nbsp;:

```
System configuration ---> Enable root login with password ---> décocher
```

**<u>Attention&nbsp;:</u>** désactiver l'authentification par mot de passe pour l'utilisateur
`root` implique une impossibilité pour les administrateurs à accéder au système
sans utiliser le protocole SSH&nbsp;!

### Activation de SELinux

SELinux est un LSM (*Linux Security Module*) permettant de définir des règles de protection
de type MAC (*Mandatory Access Control*) pour le contrôle d'accès aux données, le confinement
de programmes, les ressources réseau, etc.

Par défaut, SELinux est désactivé, pour l'activer, il suffit d'activer l'option depuis le `make menuconfig`&nbsp;:

```
Target packages ---> Enable SELinux support ---> cocher
```

### Activation de seccomp

Seccomp (*SECure COMPuting with filters*), n'est pas un composant de sécurité du type LSM
(*Linux Security Module*), mais permet d'établir un filtrage au plus près du noyau afin de
bloquer ou autoriser l'appel à une instruction particulière depuis un contexte d'exécution donné.

Le fonctionnement de seccomp s'appuie sur un filtrage BPF (*Berkeley Packet Filter*),
dispositif de filtrage utilisé notamment pour filtrer des *sockets* dans iptables,
permettant de filtrer ici les appels systèmes et leurs arguments.

Par défaut, seccomp n'est pas installé, pour l'installer, il suffit d'activer l'option depuis
le `make menuconfig`&nbsp;:

```
Target packages ---> Libraries ---> Other ---> libseccomp
```

## Authentification SSH par clé publique

Mise à jour du fichier de post-build afin de générer et ajouter à l'image système, la clé publique
qui sera utilisée pour l'authentification SSH&nbsp;:

```bash
$ cat <<-'_EOF_' >>board/raspberrypi3/post-build.sh
SSH_KEY=~/.ssh/$(git rev-parse HEAD)_rsa
[ -f ${SSH_KEY} ] && rm -f ${SSH_KEY} ${SSH_KEY}.pub
ssh-keygen -N '' -t rsa -b 4096 -o -a 100 -f ${SSH_KEY}
mkdir -p ${TARGET_DIR}/root/.ssh/
cat ${SSH_KEY}.pub >${TARGET_DIR}/root/.ssh/authorized_keys
if grep buildroot ~/.ssh/config >/dev/null; then
  perl -p -i -e 'BEGIN{undef $/;} s@(Host buildroot[^/]+.*IdentityFile ).*@${1}'${SSH_KEY}'@' ~/.ssh/config
else
  cat <<-EOF >>~/.ssh/config
Host buildroot
  Hostname 192.168.0.1
  Port 22
  User root
  IdentityFile ${SSH_KEY}

EOF
fi

_EOF_
```

Une fois reflashée, l'image devrait alors contenir le fichier `/root/.ssh/authorized_keys`
permettant l'authentification SSH au moyen d'une clé publique générée lors de la
compilation. Le fichier `~/.ssh/config` de notre système hôte devrait également
contenir la configuration permettant l'authentification SSH sur le Raspberry PI, vérifions&nbsp;:

```bash
$ ssh buildroot
```

## Conclusion

La composition d'un système embarqué GNU/Linux au moyen d'outils intuitifs tels que
Buildroot est primordiale pour s'introduire au fonctionnement d’un système d'exploitation
et pouvoir mieux appréhender les différentes contraintes issues du développement sur système
embarqué.